const fs = require("fs");
function callback(info, boardsDataPath, cb) {
  setTimeout(() => {
    if (typeof cb === "function") {
      fs.readFile(boardsDataPath, "utf-8", (err, boards) => {
        if (err) {
          cb(new Error("Path error"));
        } else {
          try {
            let arrayOfBoardsData = JSON.parse(boards);

            let finalResult = arrayOfBoardsData.filter((items) => {
              if (items.id === info) {
                return true;
              } else if (items.name === info) {
                return true;
              }
            });
            
            if (finalResult.length !== 0) {
              cb(null, finalResult);
            } else {
              cb(new Error("info not found " + info));
            }
          } catch (error) {
            cb(new Error("Data not found"));
          }
        }
      });
    } else {
      console.error("cb is not a function");
    }
  }, 2 * 1000);
}
module.exports = callback;
