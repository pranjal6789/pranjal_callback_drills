const fs = require("fs");
const path = require("path");
const boardsDataPath = path.join(__dirname, "../boards.json");
const listsDataPath = path.join(__dirname, "../lists.json");
const cardsDataPath = path.join(__dirname, "../cards.json");
function callback(cb1, cb2, cb3, cb) {
  setTimeout(() => {
    if (typeof cb === "function") {
      try {
        cb1("Thanos", boardsDataPath, function callback1(err, boards) {
          if (err) {
            cb(err.message);
          } else {
            cb(null, boards);
            let boardData = boards.find((item) => item.name === "Thanos");
            cb2(boardData.id, listsDataPath, function callback2(err, list) {
              if (err) {
                cb(err.message);
              } else {
                cb(null, list);
                for (let items of list) {
                  if (items.name === "Mind" || items.name === "Space") {
                    cb3(items.id, cardsDataPath, function callback3(err, card) {
                      if (err) {
                        cb(err.message);
                      } else {
                        cb(null, card);
                      }
                    });
                  }
                }
              }
            });
          }
        });
      } catch (error) {
        console.error("parameters not found");
      }
    } else {
      console.error("cb is not a function");
    }
  }, 2 * 1000);
}
module.exports = callback;
