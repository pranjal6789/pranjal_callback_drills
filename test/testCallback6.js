const callback1=require('../callback1.js');
const callback2=require('../callback2.js');
const callback3=require('../callback3.js');
const callback6=require('../callback6.js');
const logger=require('./logger.js');
try{
    callback6(callback1,callback2,callback3,(err,item)=>{
        if(err){
            logger.error(err);
            return;
        }else{
            logger.info(JSON.stringify(item));
            return;
        }
    });
    
}
catch(error){
    logger.error('parameter not passed');
}