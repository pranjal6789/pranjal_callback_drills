const callback1=require('../callback1.js');
const callback2=require('../callback2.js');
const callback3=require('../callback3.js');
const callback5=require('../callback5.js');
const logger=require('./logger.js');
try{
    callback5(callback1,callback2,callback3,(err,item)=>{
        if(err){
            logger.error(err);
            return;
        }else{
            logger.info(JSON.stringify(item));
            return;
        }
    });
}
catch(error){
   logger.error('arguments not passed');
}

