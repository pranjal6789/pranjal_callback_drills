const fs = require("fs");
const path = require("path");
const callback = require("../callback2.js");
const logger=require('./logger.js');

const listDataPath = path.join(__dirname, "../../lists.json");
try {
  callback("abc122dc1", listDataPath, (err, item) => {
    if (err) {
      logger.error(err.message);
    } else {
      logger.info(JSON.stringify(item));
    }
  });
} catch (error) {
  logger.error("Undefined path");
}
