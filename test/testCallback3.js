const fs = require("fs");
const path = require("path");
const callback = require("../callback3.js");
const cardsDataPath = path.join(__dirname, "../../cards.json");
const logger=require("./logger.js");

try{
  callback("jwkh245", cardsDataPath,(err,item)=>{
    if(err){
      logger.error(err.message);
    }
    else{
      logger.info(JSON.stringify(item));
    }
  })
}
catch(error){
  logger.error('undefined path');
}
