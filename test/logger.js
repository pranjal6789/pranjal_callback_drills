const { format, createLogger, transports } = require("winston");
const logFormat = format.printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

const logger = createLogger({
  level: "info",
  transports: [
    new transports.Console({
      format: format.combine(
        format.json(),
        format.timestamp(),
        format.colorize(),
        format.splat(),
        logFormat,
        
      ),
    }),
  ],
});
module.exports = logger;
