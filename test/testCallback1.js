const callback = require("../callback1.js");
const fs = require("fs");
const path = require("path");
const logger=require('./logger.js');
const boardDataPath = path.join(__dirname, "../../boards.json");

try {
  callback("mcu453ed", boardDataPath,(err, item) => {
    if (err) {
      logger.error(err.message);
    } else {
      logger.info(JSON.stringify(item));
    }
  });
} catch (error) {
  logger.error("undefined path");
}
module.exports=logger;


