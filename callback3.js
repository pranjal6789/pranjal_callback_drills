const fs = require("fs");
function callback(id, cardsDataPath, cb) {
  setTimeout(() => {
    if (typeof cb === "function") {
      fs.readFile(cardsDataPath, "utf-8", (err, cards) => {
        if (err) {
          console.error("Path error");
        } else {
          try {
            let cardsData = JSON.parse(cards);
            if (cardsData[id] !== undefined) {
              cb(null, cardsData[id]);
              return;
            } else {
              cb(Error("Id not found " + id));
              return;
            }
          } catch (error) {
            cb(Error("Data not found"));
          }
        }
      });
    } else {
      cb(Error("cb is not a function"));
    }
  }, 2 * 1000);
}
module.exports = callback;
