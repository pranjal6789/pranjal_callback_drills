const fs = require("fs");

function callback(id, listDataPath, cb) {
  setTimeout(() => {
    if (typeof cb === "function") {
      fs.readFile(listDataPath, "utf-8", (err, lists) => {
        if (err) {
          console.error("Path error");
        } else {
          try {
            let listsData = JSON.parse(lists);
            if (listsData[id] !== undefined) {
              cb(null, listsData[id]);
              return;
            } else {
              cb(new Error("Id not found " + id));
              return;
            }
          } catch (error) {
            console.error(new Error("Data not found"));
          }
        }
      });
    } else {
      console.error(new Error("cb is not a function"));
    }
  }, 2 * 1000);
}
module.exports = callback;
