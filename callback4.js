const fs = require("fs");
const path = require("path");
const boardsDataPath = path.join(__dirname, "../boards.json");
const listsDataPath = path.join(__dirname, "../lists.json");
const cardsDataPath = path.join(__dirname, "../cards.json");
function callback(cb1, cb2, cb3, cb) {
  setTimeout(() => {
    if (typeof cb === "function") {
      try {
        cb1("Thanos", boardsDataPath, function callback1(err, boards) {
          if (err) {
            cb(err.message);
          } else {
            cb(null, boards);
            let boardsData = boards.find((item) => item.name === "Thanos");
            cb2(boardsData.id, listsDataPath, function callback2(err, list) {
              if (err) {
                cb(err.message);
              } else {
                cb(null, list);
                for (let items of list) {
                  if (items.name === "Mind") {
                    cb3(items.id, cardsDataPath, function callback3(err, card) {
                      if (err) {
                        cb(err.message);
                      } else {
                        cb(null, card);
                      }
                    });
                  }
                }
              }
            });
          }
        });
      } catch (error) {
        console.error("Parameters not passed");
      }
    } else {
      console.error("cb is not a function");
    }
  }, 2 * 1000);
}
module.exports = callback;
